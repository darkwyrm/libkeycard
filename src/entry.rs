use chrono::prelude::*;
use chrono::{Duration, NaiveDate};
use eznacl::*;
use std::collections::HashMap;
use std::fmt;

use crate::base::*;
use crate::verifiers::*;

static ORG_PERMITTED_FIELDS: [&str; 14] = [
    "Type",
    "Index",
    "Name",
    "Domain",
    "Contact-Admin",
    "Contact-Abuse",
    "Contact-Support",
    "Language",
    "Primary-Verification-Key",
    "Secondary-Verification-Key",
    "Encryption-Key",
    "Time-To-Live",
    "Expires",
    "Timestamp",
];

static ORG_REQUIRED_FIELDS: [&str; 10] = [
    "Type",
    "Index",
    "Name",
    "Domain",
    "Contact-Admin",
    "Primary-Verification-Key",
    "Encryption-Key",
    "Time-To-Live",
    "Expires",
    "Timestamp",
];

static USER_PERMITTED_FIELDS: [&str; 13] = [
    "Type",
    "Index",
    "Name",
    "User-ID",
    "Workspace-ID",
    "Domain",
    "Contact-Request-Verification-Key",
    "Contact-Request-Encryption-Key",
    "Encryption-Key",
    "Verification-Key",
    "Time-To-Live",
    "Expires",
    "Timestamp",
];

static USER_REQUIRED_FIELDS: [&str; 11] = [
    "Type",
    "Index",
    "Workspace-ID",
    "Domain",
    "Contact-Request-Verification-Key",
    "Contact-Request-Encryption-Key",
    "Encryption-Key",
    "Verification-Key",
    "Time-To-Live",
    "Expires",
    "Timestamp",
];

/// Denotes an entry's type. None is only used for uninitialized Entry instances.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub enum EntryType {
    Organization,
    User,
    None,
}

impl fmt::Display for EntryType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EntryType::None => write!(f, "None"),
            EntryType::Organization => write!(f, "Organization"),
            EntryType::User => write!(f, "User"),
        }
    }
}

/// Entry represents a single entry in a keycard and contains both fields and authentication
/// strings, which can be a digital signature or a cryptographic hash.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Entry {
    _type: EntryType,
    fields: HashMap<String, String>,
    sigs: HashMap<String, CryptoString>,
}

impl Entry {
    /// Creates a new entry given the value held in the passed string. As if this writing only
    /// "Organization", "User", or "" are valid, the last of which creating an Entry of type None.
    pub fn new_from_str(entrytype: &str) -> Result<Entry, LKCError> {
        match entrytype {
            "Organization" => Entry::new(EntryType::Organization),
            "User" => Entry::new(EntryType::User),
            "" => Entry::new(EntryType::None),
            _ => Err(LKCError::ErrBadValue),
        }
    }

    /// Creates a new entry based on the type given
    pub fn new(entrytype: EntryType) -> Result<Entry, LKCError> {
        let mut out = Entry {
            _type: entrytype,
            fields: HashMap::<String, String>::new(),
            sigs: HashMap::<String, CryptoString>::new(),
        };

        match entrytype {
            EntryType::Organization => out.set_field("Type", "Organization")?,
            EntryType::User => out.set_field("Type", "User")?,
            EntryType::None => (),
        }

        // Set some default values to save the caller some time.
        out.set_field("Timestamp", &get_timestamp())?;
        out.set_field("Time-To-Live", "14")?;
        match entrytype {
            EntryType::User => out.set_expiration(90)?,
            EntryType::Organization => out.set_expiration(365)?,
            _ => (),
        }

        Ok(out)
    }

    /// Creates a new entry from the text data given it. The format of an entry is documented in
    /// the Mensago
    /// [Identity Services](https://gitlab.com/mensago/mensago-docs/-/blob/master/Identity%20Services.adoc)
    /// design document.
    pub fn from(s: &str) -> Result<Entry, LKCError> {
        // 160 is a close approximation. It includes the names of all required fields and the
        // minimum length for any variable-length fields, including keys. It's a good quick way of
        // ruling out obviously bad data.
        if s.len() < 160 {
            return Err(LKCError::ErrBadValue);
        }

        let mut out = Entry::new(EntryType::None)?;
        for line in s.split("\r\n") {
            if line.len() == 0 {
                continue;
            }

            let trimmed = line.trim();
            if trimmed.len() == 0 {
                continue;
            }

            let parts = trimmed.splitn(2, ":").collect::<Vec<&str>>();
            if parts.len() != 2 {
                return Err(LKCError::ErrBadFieldValue(String::from(trimmed)));
            }

            if parts[1].len() > 6144 {
                return Err(LKCError::ErrOutOfRange);
            }

            let field_value = match parts.get(1) {
                Some(v) => v.clone(),
                None => return Err(LKCError::ErrBadFieldValue(String::from(parts[0]))),
            };

            match parts[0] {
                "Custody-Signature"
                | "Organization-Signature"
                | "Previous-Hash"
                | "Hash"
                | "User-Signature" => match CryptoString::from(field_value) {
                    Some(cs) => {
                        out.add_authstr(parts[0], &cs)?;
                        continue;
                    }
                    None => return Err(LKCError::ErrBadFieldValue(String::from(parts[0]))),
                },
                _ => (),
            }

            match out.set_field(parts[0], field_value) {
                Ok(_) => { /* */ }
                Err(e) => return Err(e),
            }
        }

        Ok(out)
    }

    /// Returns true if the entry has a specific field
    pub fn has_field(&self, field: &str) -> bool {
        match self.fields.get(field) {
            Some(_) => true,
            None => false,
        }
    }

    /// Gets the specified field for an entry. Naming for the field exactly matches the spec.
    pub fn get_field(&self, field: &str) -> Result<String, LKCError> {
        match self.fields.get(field) {
            Some(v) => Ok(v.clone()),
            None => Err(LKCError::ErrNotFound),
        }
    }

    /// Sets an entry field. Naming for the field exactly matches the spec.
    pub fn set_field(&mut self, field: &str, value: &str) -> Result<(), LKCError> {
        verify_field(field, value)?;

        if field == "Type" {
            self._type = match value {
                "Organization" => EntryType::Organization,
                "User" => EntryType::User,
                _ => EntryType::None,
            };
        }
        let _ = self.fields.insert(String::from(field), String::from(value));
        Ok(())
    }

    /// Sets multiple entry fields from a list of type-value mappings
    pub fn set_fields(&mut self, fields: &Vec<(String, String)>) -> Result<(), LKCError> {
        for (k, v) in fields.iter() {
            self.set_field(&k, &v)?;
        }
        Ok(())
    }

    /// Deletes a field from the entry
    pub fn delete_field(&mut self, field: &str) -> Result<(), LKCError> {
        let _ = self.fields.remove(field);
        Ok(())
    }

    /// Returns the owner for the entry, which will a string containing a workspace address for a
    /// user entry and a domain for an organization entry. It will fail if the needed fields are
    /// not populated (Doman, Domain + Workspace-ID).
    pub fn get_owner(&self) -> Result<String, LKCError> {
        match self._type {
            EntryType::Organization => self.get_field("Domain"),
            EntryType::User => {
                let mut widstr = self.get_field("Workspace-ID")?;
                let domain = self.get_field("Domain")?;
                widstr.push('/');
                widstr.push_str(&domain);

                Ok(widstr)
            }
            _ => return Err(LKCError::ErrNoInit),
        }
    }

    /// Checks the formatting of the regular fields in the entry and returns false if a field does
    /// not comply. This method is usually called to ensure that the data in an entry is valid
    /// before proceeding with the signing and hashing process.
    pub fn is_data_compliant(&self) -> Result<(), LKCError> {
        // Ensure that all required fields are present. Because each field is a ValidatedString, we
        // already know that if the field is present, it's valid, too. :)
        match self._type {
            EntryType::Organization => {
                for f in ORG_REQUIRED_FIELDS {
                    match self.fields.get(f) {
                        Some(_) => { /* do nothing */ }
                        None => return Err(LKCError::ErrMissingField(String::from(f))),
                    }
                }
            }
            EntryType::User => {
                for f in USER_REQUIRED_FIELDS {
                    match self.fields.get(f) {
                        Some(_) => { /* do nothing */ }
                        None => return Err(LKCError::ErrMissingField(String::from(f))),
                    }
                }
            }
            _ => return Err(LKCError::ErrNoInit),
        }

        Ok(())
    }

    /// Returns false if the entry has any compliance issues, including missing or bad hashes
    /// and/or signatures. This method performs all the checks made in `is_data_compliant()` and
    /// more. Note that only the format of signatures and hashes are checked. The validity of a
    /// hash or signature must be checked using [`verify()`](struct.Keycard.html#method.verify) or
    /// [`verify_chain()`](struct.Keycard.html#method.verify_chain).
    ///
    /// For an entry to be compliant, an organization entry MUST have the following fields:
    ///
    /// - Type
    /// - Index
    /// - Name
    /// - Domain
    /// - Contact-Admin
    /// - Primary-Verification-Key
    /// - Encryption-Key
    /// - Time-To-Live"
    /// - Expires
    /// - Timestamp
    ///
    ///
    /// Organizational entries may also have any of the following optional fields:
    ///
    /// - Contact-Abuse
    /// - Contact-Support
    /// - Language
    /// - Secondary-Verification-Key
    ///
    /// User entries MUST have the following fields:
    ///
    /// - Type
    /// - Index
    /// - Workspace-ID
    /// - Domain
    /// - Contact-Request-Verification-Key
    /// - Contact-Request-Encryption-Key
    /// - Verification-Key
    /// - Encryption-Key
    /// - Time-To-Live"
    /// - Expires
    /// - Timestamp
    ///
    /// User entries MAY also have a Name or User-ID field, although these are optional.
    ///
    /// Additionally, any entry MUST also have signatures and hashes applied in the order specified
    /// in the description for [`get_full_text()`](struct.Entry.html#method.get_full_text).
    pub fn is_compliant(&self) -> Result<(), LKCError> {
        self.is_data_compliant()?;

        match self._type {
            EntryType::Organization => {
                if self.get_field("Index")? == "1" {
                    // An organization's first (and hopefully only) root keycard should *never*
                    // have a Revoke field.
                    if self.has_field("Revoke") {
                        return Err(LKCError::ErrInvalidKeycard);
                    }
                } else {
                    // The only time an org entry which has an Index greater than one should not
                    // have a custody signature or a previous hash is if the previous entry was
                    // revoked and, thus, the current one is the new root for the organization.
                    if !self.has_field("Revoke") {
                        _ = self.get_authstr("Custody-Signature")?;
                        _ = self.get_authstr("Previous-Hash")?;
                    }
                }
                _ = self.get_authstr("Hash")?;
                _ = self.get_authstr("Organization-Signature")?;
            }
            EntryType::User => {
                if self.get_field("Index")? == "1" {
                    // A user's first root keycard should *never* have a Revoke field.
                    if self.has_field("Revoke") {
                        return Err(LKCError::ErrInvalidKeycard);
                    }
                } else {
                    // A replacement root entry will have a Revoke field, in which case it will
                    // not have a custody signature, but it will have an index greater than 1.
                    if !self.has_field("Revoke") {
                        _ = self.get_authstr("Custody-Signature")?;
                    }
                }
                _ = self.get_authstr("Organization-Signature")?;
                _ = self.get_authstr("Previous-Hash")?;
                _ = self.get_authstr("Hash")?;
                _ = self.get_authstr("User-Signature")?;
            }
            EntryType::None => return Err(LKCError::ErrNoInit),
        }

        Ok(())
    }

    /// Sets the expiration date for the entry. The maximum number of days for entries is 1095
    /// (~3 years). The recommended value are 365 for an organization entry and 90 for a user entry.
    pub fn set_expiration(&mut self, numdays: u16) -> Result<(), LKCError> {
        if numdays > 1095 || numdays < 1 {
            return Err(LKCError::ErrBadValue);
        }

        let offdate = match get_offset_date(Duration::days(numdays as i64)) {
            Some(v) => v,
            None => {
                return Err(LKCError::ErrProgramException(String::from(
                    "set_expiration: failure to create expiration date",
                )))
            }
        };
        self.set_field("Expires", &offdate)?;

        Ok(())
    }

    /// Returns true if the entry has exceeded its expiration date
    pub fn is_expired(&self) -> Result<bool, LKCError> {
        // Yes, it would make more sense to simply have a stored value which was already parsed,
        // but the necessary code to do the dynamic dispatch to handle this would probably increase
        // complexity by an order of magnitude. We'll sacrifice a tiny bit of performance for
        // simplicity.
        let expdate = match self.fields.get("Expires") {
            Some(v) => {
                match NaiveDate::parse_from_str(v, "%Y%m%d") {
                    Ok(d) => d,
                    Err(e) => {
                        // We should never be here
                        return Err(LKCError::ErrProgramException(e.to_string()));
                    }
                }
            }
            None => return Err(LKCError::ErrNotFound),
        };

        let now = Utc::now().date_naive();

        Ok(now > expdate)
    }

    /// Returns the body text of the entry
    pub fn get_text(&self) -> Result<String, LKCError> {
        let mut lines = Vec::<String>::new();

        // Ensure that all required fields are present. Because each field is validated at time
        // of assignment, we know that each one is valid if present.
        match self._type {
            EntryType::Organization => {
                for i in ORG_PERMITTED_FIELDS.iter() {
                    match self.fields.get(*i) {
                        Some(v) => {
                            let parts = [*i, v];
                            lines.push(parts.join(":"));
                        }
                        None => { /* */ }
                    }
                }
            }
            EntryType::User => {
                for i in USER_PERMITTED_FIELDS.iter() {
                    match self.fields.get(*i) {
                        Some(v) => {
                            let parts = [*i, v];
                            lines.push(parts.join(":"));
                        }
                        None => { /* */ }
                    }
                }
            }
            _ => return Err(LKCError::ErrNoInit),
        };

        // Keycards are expected to end with a blank line
        lines.push(String::from(""));

        Ok(lines.join("\r\n"))
    }

    /// Returns the full text of the entry, including signatures, up to but not including the one
    /// specified. Passing an empty string as the signature level will result in the entire entry
    /// being returned.
    ///
    /// The order for organization entries:
    ///
    /// - Custody-Signature
    /// - Previous-Hash
    /// - Hash
    /// - Organization-Signature
    ///
    /// The order for user entries:
    ///
    /// - Custody-Signature
    /// - Organization-Signature
    /// - Previous-Hash
    /// - Hash
    /// - User-Signature
    pub fn get_full_text(&self, siglevel: &str) -> Result<String, LKCError> {
        match self._type {
            EntryType::Organization => self.org_get_full_text(siglevel),
            EntryType::User => self.user_get_full_text(siglevel),
            _ => return Err(LKCError::ErrNoInit),
        }
    }

    /// Returns true if the supplied AuthStr is populated and valid
    pub fn has_authstr(&self, astype: &str) -> bool {
        match self.sigs.get(astype) {
            Some(_) => true,
            None => false,
        }
    }

    /// Returns the specified authentication string
    pub fn get_authstr(&self, astype: &str) -> Result<&CryptoString, LKCError> {
        match self.sigs.get(astype) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrMissingField(String::from(astype))),
        }
    }

    /// Sets the specified authentication string to the value passed. NOTE: no validation of the
    /// authentication string is performed by this call. The primary use for this method is to set
    /// the Previous-Hash for the entry
    pub fn add_authstr(&mut self, astype: &str, astr: &CryptoString) -> Result<(), LKCError> {
        verify_authstr(astype, astr)?;

        let _ = self.sigs.insert(String::from(astype), astr.clone());
        Ok(())
    }

    /// Creates the requested signature. Requirements for this call vary with the entry
    /// implementation. ErrOutOfOrderSignature is returned if a signature is requested before
    /// another required authentication string has been set. ErrBadValue is returned for a
    /// signature type not used by the specific implementation.
    pub fn sign(&mut self, astype: &str, signing_pair: &SigningPair) -> Result<(), LKCError> {
        let totaldata = self.get_full_text(astype)?;
        let signature = signing_pair.sign(totaldata.as_bytes())?;
        self.add_authstr(&astype, &signature)
    }

    /// Verifies the requested signature. ErrBadValue is returned for a signature type not used by
    /// the specific implementation. ErrVerificationFailure is returned if the signature fails to
    /// verify
    pub fn verify_signature<K: VerifySignature>(
        &self,
        astype: &str,
        verify_key: &K,
    ) -> Result<(), LKCError> {
        let sig = self.get_authstr(astype)?;
        let totaldata = match self._type {
            EntryType::Organization => self.org_get_full_text(astype)?,
            EntryType::User => self.user_get_full_text(astype)?,
            EntryType::None => return Err(LKCError::ErrNoInit),
        };

        if verify_key.verify(totaldata.as_bytes(), &sig)? {
            Ok(())
        } else {
            Err(LKCError::ErrVerificationFailure)
        }
    }

    /// Calculates the hash for the entry text using the specified algorithm. For information on
    /// signature order, please see [`get_full_text()`](struct.Keycard.html#method.get_full_text).
    /// All signatures are required except for Custody-Signature and Previous-Hash, which are not
    /// required for an organization's root keycard entry. ErrOutOfOrderSignature is returned if a
    /// hash is requested before another required authentication string has been set.
    pub fn hash(&mut self, algorithm: &str) -> Result<(), LKCError> {
        let totaldata = self.get_full_text("Hash")?;
        let hash = get_hash(algorithm, totaldata.as_bytes())?;
        self.add_authstr("Hash", &hash)
    }

    /// Verifies the data of the entry with the hash currently assigned. Returns Ok on success and
    /// ErrHashMismatch on failure.
    pub fn verify_hash(&self) -> Result<(), LKCError> {
        let currenthash = self.get_authstr("Hash")?;

        let totaldata = self.get_full_text("Hash")?;
        let hash = get_hash(currenthash.prefix(), totaldata.as_bytes())?;

        if *currenthash == hash {
            Ok(())
        } else {
            Err(LKCError::ErrHashMismatch)
        }
    }

    /// Creates a new Entry object with new keys and a custody signature. It requires the contact
    /// request signing keypair used for the entry so that the Custody-Signature field is
    /// generated correctly. For handling of expiration date, see
    /// [`set_expiration()`](struct.Keycard.html#method.set_expiration).
    pub fn chain(
        &self,
        spair: &SigningPair,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        match self._type {
            EntryType::Organization => self.org_chain(spair, expires),
            EntryType::User => self.user_chain(spair, expires),
            EntryType::None => Err(LKCError::ErrNoInit),
        }
    }

    /// Verifies the chain of custody between the provided entry and the current one. If either
    /// card is invalid, ErrInvalidKeycard is returned. If the index of entry is not the
    /// immediate successor to the previous one, ErrBadValue is returned.
    pub fn verify_chain(&self, previous: &Entry) -> Result<(), LKCError> {
        previous.is_compliant()?;
        self.is_compliant()?;

        match self._type {
            EntryType::Organization => self.org_verify_chain(previous),
            EntryType::User => self.user_verify_chain(previous),
            EntryType::None => Err(LKCError::ErrNoInit),
        }
    }

    /// This method is called when the current entry must be revoked because one or more keys were
    /// compromised. A new root entry is created with a `Revoke` field containing the hash of the
    /// current one and an `Index` which is, like `chain()`, one greater than the current entry. For
    /// handling of the expiration interval, see
    /// [`set_expiration()`](struct.Keycard.html#method.set_expiration).
    pub fn revoke(
        &self,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        self.is_compliant()?;

        match self._type {
            EntryType::Organization => self.org_revoke(expires),
            EntryType::User => self.user_revoke(expires),
            EntryType::None => Err(LKCError::ErrNoInit),
        }
    }

    /// Makes a careful duplicate of the entry. Note that this is NOT the same as the standard
    /// library trait of the same name. This method handles the data duplication needs for chaining
    /// together two entries, excluding certain fields and handling expiration carefully.
    fn copy(&self) -> Self {
        let entrytype = self
            .get_field("Type")
            .expect("Failed to get type in Entry::copy()");

        let mut out = Entry::new_from_str(&entrytype).expect("Failed to create new ");

        for (k, v) in self.fields.iter() {
            match k.as_str() {
                "Index"
                | "PrimaryVerificationKey"
                | "SecondaryVerificationKey"
                | "EncryptionKey"
                | "Expires"
                | "Timestamp" => { /* Field is set correctly in new(). Do nothing. */ }
                _ => {
                    out.set_field(k, v)
                        .expect("Failed to copy field in OrgEntry::copy()");
                }
            }
        }

        // The copy has an Index value of one greater than the original

        let index = self
            .get_field("Index")
            .expect("Missing Index field in OrgEntry::copy()");
        let new_index = increment_index_string(&index)
            .expect("Failed to increment Index field in OrgEntry::copy()");
        out.set_field("Index", &new_index)
            .expect("Failed to set Index field to new value in OrgEntry::copy()");

        out
    }

    // Private methods for type-specific functionality

    fn org_get_full_text(&self, siglevel: &str) -> Result<String, LKCError> {
        let mut strings = Vec::<String>::new();
        strings.push(self.get_text()?);

        let require_previous = self.get_field("Index")? != "1" && !self.has_field("Revoke");

        match siglevel {
            "User-Signature" => {
                // User-Signature doesn't exist in an org entry
                return Err(LKCError::ErrBadValue);
            }
            "Custody-Signature" => {
                /* For the custody signature, we don't need to do anything extra */
            }
            "Previous-Hash" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
            }
            "Hash" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
            }
            "Organization-Signature" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
                match &self.sigs.get("Hash") {
                    Some(v) => {
                        strings.push(format!("Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Hash")));
                        }
                    }
                };
            }
            "" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
                match &self.sigs.get("Hash") {
                    Some(v) => {
                        strings.push(format!("Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Hash")));
                        }
                    }
                };
                match &self.sigs.get("Organization-Signature") {
                    Some(v) => {
                        strings.push(format!("Organization-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        return Err(LKCError::ErrMissingField(String::from(
                            "Organization-Signature",
                        )))
                    }
                };
            }
            _ => return Err(LKCError::ErrBadValue),
        };

        Ok(strings.join(""))
    }

    fn user_get_full_text(&self, siglevel: &str) -> Result<String, LKCError> {
        let mut strings = Vec::<String>::new();
        strings.push(self.get_text()?);

        let require_previous = self.get_field("Index")? != "1" && !self.has_field("Revoke");

        match siglevel {
            "Custody-Signature" => {
                /* For the custody signature, we don't need to do anything extra */
            }
            "Organization-Signature" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
            }
            "Previous-Hash" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Organization-Signature") {
                    Some(v) => {
                        strings.push(format!("Organization-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        return Err(LKCError::ErrMissingField(String::from(
                            "Organization-Signature",
                        )))
                    }
                };
            }
            "Hash" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Organization-Signature") {
                    Some(v) => {
                        strings.push(format!("Organization-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        return Err(LKCError::ErrMissingField(String::from(
                            "Organization-Signature",
                        )))
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
            }
            "User-Signature" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Organization-Signature") {
                    Some(v) => {
                        strings.push(format!("Organization-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        return Err(LKCError::ErrMissingField(String::from(
                            "Organization-Signature",
                        )))
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
                match &self.sigs.get("Hash") {
                    Some(v) => {
                        strings.push(format!("Hash:{}\r\n", v.to_string()));
                    }
                    None => return Err(LKCError::ErrMissingField(String::from("Hash"))),
                };
            }
            "" => {
                match &self.sigs.get("Custody-Signature") {
                    Some(v) => {
                        strings.push(format!("Custody-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from(
                                "Custody-Signature",
                            )));
                        }
                    }
                };
                match &self.sigs.get("Organization-Signature") {
                    Some(v) => {
                        strings.push(format!("Organization-Signature:{}\r\n", v.to_string()));
                    }
                    None => {
                        return Err(LKCError::ErrMissingField(String::from(
                            "Organization-Signature",
                        )))
                    }
                };
                match &self.sigs.get("Previous-Hash") {
                    Some(v) => {
                        strings.push(format!("Previous-Hash:{}\r\n", v.to_string()));
                    }
                    None => {
                        if require_previous {
                            return Err(LKCError::ErrMissingField(String::from("Previous-Hash")));
                        }
                    }
                };
                match &self.sigs.get("Hash") {
                    Some(v) => {
                        strings.push(format!("Hash:{}\r\n", v.to_string()));
                    }
                    None => return Err(LKCError::ErrMissingField(String::from("Hash"))),
                };
                match &self.sigs.get("User-Signature") {
                    Some(v) => {
                        strings.push(format!("User-Signature:{}\r\n", v.to_string()));
                    }
                    None => return Err(LKCError::ErrMissingField(String::from("User-Signature"))),
                };
            }
            _ => return Err(LKCError::ErrBadValue),
        };

        Ok(strings.join(""))
    }

    fn org_chain(
        &self,
        spair: &SigningPair,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        let mut map = HashMap::<&str, CryptoString>::new();
        let mut entry = self.copy();

        // TODO: add preferred algorithm support once supported in eznacl
        let newspair = SigningPair::generate("ED25519")?;
        map.insert(
            "primary.public",
            CryptoString::from(&newspair.get_public_str())
                .expect("Error getting inserting primary verification key in OrgEntry::chain()"),
        );
        map.insert(
            "primary.private",
            CryptoString::from(&newspair.get_private_str())
                .expect("Error getting inserting primary signing key in OrgEntry::chain()"),
        );
        match entry.set_field("Primary-Verification-Key", &newspair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting primary verification key in OrgEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let epair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "encryption.public",
            CryptoString::from(&epair.get_public_str())
                .expect("Error getting inserting encryption key in OrgEntry::chain()"),
        );
        map.insert(
            "encryption.private",
            CryptoString::from(&epair.get_private_str())
                .expect("Error getting inserting decryption key in OrgEntry::chain()"),
        );
        match entry.set_field("Encryption-Key", &epair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting encryption key in OrgEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        // Now that we have a new primary signing pair, move the old one over to secondary
        match entry.set_field(
            "Secondary-Verification-Key",
            &self.get_field("Primary-Verification-Key").expect(""),
        ) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error moving primary key to secondary in OrgEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        let hash = self.get_authstr("Hash")?;
        entry.add_authstr("Previous-Hash", hash)?;
        entry.set_expiration(expires)?;
        entry.sign("Custody-Signature", &spair)?;
        entry.hash(hash.prefix())?;
        entry.sign("Organization-Signature", &newspair)?;

        Ok((entry, map))
    }

    fn user_chain(
        &self,
        spair: &SigningPair,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        let mut map = HashMap::<&str, CryptoString>::new();
        let mut entry = self.copy();

        // TODO: add preferred algorithm support once supported in eznacl
        let newcrspair = SigningPair::generate("ED25519")?;
        map.insert(
            "crsigning.public",
            CryptoString::from(&newcrspair.get_public_str())
                .expect("Error getting inserting CR verification key in UserEntry::chain()"),
        );
        map.insert(
            "crsigning.private",
            CryptoString::from(&newcrspair.get_private_str())
                .expect("Error getting inserting CR signing key in UserEntry::chain()"),
        );
        match entry.set_field(
            "Contact-Request-Verification-Key",
            &newcrspair.get_public_str(),
        ) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting CR verification key in UserEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newcrepair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "crencryption.public",
            CryptoString::from(&newcrepair.get_public_str())
                .expect("Error getting inserting CR encryption key in UserEntry::chain()"),
        );
        map.insert(
            "crencryption.private",
            CryptoString::from(&newcrepair.get_private_str())
                .expect("Error getting inserting CR decryption key in UserEntry::chain()"),
        );
        match entry.set_field(
            "Contact-Request-Encryption-Key",
            &newcrepair.get_public_str(),
        ) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting CR encryption key in UserEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newspair = SigningPair::generate("ED25519")?;
        map.insert(
            "signing.public",
            CryptoString::from(&newspair.get_public_str())
                .expect("Error getting inserting verification key in UserEntry::chain()"),
        );
        map.insert(
            "signing.private",
            CryptoString::from(&newspair.get_private_str())
                .expect("Error getting inserting signing key in UserEntry::chain()"),
        );
        match entry.set_field("Verification-Key", &newspair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting verification key in UserEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newepair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "encryption.public",
            CryptoString::from(&newepair.get_public_str())
                .expect("Error getting inserting encryption key in UserEntry::chain()"),
        );
        map.insert(
            "encryption.private",
            CryptoString::from(&newepair.get_private_str())
                .expect("Error getting inserting decryption key in UserEntry::chain()"),
        );
        match entry.set_field("Encryption-Key", &newepair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting encryption key in UserEntry::chain(): {}",
                    e.to_string()
                )))
            }
        }

        let hash = self.get_authstr("Hash")?;
        entry.add_authstr("Previous-Hash", hash)?;
        entry.set_expiration(expires)?;
        entry.sign("Custody-Signature", &spair)?;

        Ok((entry, map))
    }

    fn org_verify_chain(&self, previous: &Entry) -> Result<(), LKCError> {
        if previous._type != EntryType::Organization {
            return Err(LKCError::ErrTypeMismatch);
        }

        // Make sure that the previous entry is the immediate predecessor of the current one
        let previndex = match previous.get_field("Index") {
            Ok(v) => v,
            Err(_) => return Err(LKCError::ErrInvalidKeycard),
        };
        let currentindex = match self.get_field("Index") {
            Ok(v) => v,
            Err(_) => return Err(LKCError::ErrInvalidKeycard),
        };
        match increment_index_string(&previndex) {
            Ok(v) => {
                if v != currentindex {
                    return Err(LKCError::ErrBadValue);
                }
            }
            Err(_) => return Err(LKCError::ErrBadFieldValue(String::from("Index"))),
        }

        let verkeystr = previous.get_field("Primary-Verification-Key")?;
        let verkey = match VerificationKey::from_string(&verkeystr) {
            Some(v) => v,
            None => return Err(LKCError::ErrInvalidKey),
        };

        self.verify_signature("Custody-Signature", &verkey)
    }

    fn user_verify_chain(&self, previous: &Entry) -> Result<(), LKCError> {
        // The previous entry for a root user entry must be an organization entry -- the org
        // keycard's branch point
        match previous._type {
            EntryType::User => {
                if self.get_field("Index")? == "1" {
                    return Err(LKCError::ErrTypeMismatch);
                }
            }
            EntryType::Organization => {
                if self.get_field("Index")? != "1" && !self.has_field("Revoke") {
                    return Err(LKCError::ErrTypeMismatch);
                }
            }
            _ => return Err(LKCError::ErrTypeMismatch),
        }

        // Make sure that the two are hash linked and that the Index field is sequential unless
        // we're dealing with a branch point
        if previous.get_authstr("Hash")? != self.get_authstr("Previous-Hash")? {
            return Err(LKCError::ErrHashMismatch);
        }

        if previous.get_field("Type")? == self.get_field("Type")? {
            let previndex = match previous.get_field("Index") {
                Ok(v) => v,
                Err(_) => return Err(LKCError::ErrInvalidKeycard),
            };
            let currentindex = match self.get_field("Index") {
                Ok(v) => v,
                Err(_) => return Err(LKCError::ErrInvalidKeycard),
            };
            match increment_index_string(&previndex) {
                Ok(v) => {
                    if v != currentindex {
                        return Err(LKCError::ErrBadValue);
                    }
                }
                Err(_) => return Err(LKCError::ErrBadFieldValue(String::from("Index"))),
            }
        }

        match previous._type {
            EntryType::User => {
                let verkeystr = previous.get_field("Contact-Request-Verification-Key")?;
                let verkey = match VerificationKey::from_string(&verkeystr) {
                    Some(v) => v,
                    None => return Err(LKCError::ErrInvalidKey),
                };
                self.verify_signature("Custody-Signature", &verkey)
            }
            EntryType::Organization => {
                let verkeystr = previous.get_field("Primary-Verification-Key")?;
                let verkey = match VerificationKey::from_string(&verkeystr) {
                    Some(v) => v,
                    None => return Err(LKCError::ErrInvalidKey),
                };
                self.verify_signature("Organization-Signature", &verkey)
            }
            _ => unreachable!(),
        }
    }

    fn org_revoke(
        &self,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        let mut map = HashMap::<&str, CryptoString>::new();
        let mut entry = self.copy();

        // TODO: add preferred algorithm support once supported in eznacl
        let newspair = SigningPair::generate("ED25519")?;
        map.insert(
            "primary.public",
            CryptoString::from(&newspair.get_public_str())
                .expect("Error getting inserting primary verification key in org_revoke()"),
        );
        map.insert(
            "primary.private",
            CryptoString::from(&newspair.get_private_str())
                .expect("Error getting inserting primary signing key in org_revoke()"),
        );
        match entry.set_field("Primary-Verification-Key", &newspair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting primary verification key in org_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let epair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "encryption.public",
            CryptoString::from(&epair.get_public_str())
                .expect("Error getting inserting encryption key in org_revoke()"),
        );
        map.insert(
            "encryption.private",
            CryptoString::from(&epair.get_private_str())
                .expect("Error getting inserting decryption key in org_revoke()"),
        );
        match entry.set_field("Encryption-Key", &epair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting encryption key in org_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        // This new entry has no custody signature--it's a new root entry.

        let hash = self.get_authstr("Hash")?;
        entry.set_field("Revoke", hash.as_str())?;
        entry.set_expiration(expires)?;
        entry.hash(hash.prefix())?;
        entry.sign("Organization-Signature", &newspair)?;

        Ok((entry, map))
    }

    fn user_revoke(
        &self,
        expires: u16,
    ) -> Result<(Entry, HashMap<&'static str, CryptoString>), LKCError> {
        let mut map = HashMap::<&str, CryptoString>::new();
        let mut entry = self.copy();

        // TODO: add preferred algorithm support once supported in eznacl
        let newcrspair = SigningPair::generate("ED25519")?;
        map.insert(
            "crsigning.public",
            CryptoString::from(&newcrspair.get_public_str())
                .expect("Error getting inserting CR verification key in user_revoke()"),
        );
        map.insert(
            "crsigning.private",
            CryptoString::from(&newcrspair.get_private_str())
                .expect("Error getting inserting CR signing key in user_revoke()"),
        );
        match entry.set_field(
            "Contact-Request-Verification-Key",
            &newcrspair.get_public_str(),
        ) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting CR verification key in user_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newcrepair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "crencryption.public",
            CryptoString::from(&newcrepair.get_public_str())
                .expect("Error getting inserting CR encryption key in user_revoke()"),
        );
        map.insert(
            "crencryption.private",
            CryptoString::from(&newcrepair.get_private_str())
                .expect("Error getting inserting CR decryption key in user_revoke()"),
        );
        match entry.set_field(
            "Contact-Request-Encryption-Key",
            &newcrepair.get_public_str(),
        ) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting CR encryption key in user_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newspair = SigningPair::generate("ED25519")?;
        map.insert(
            "signing.public",
            CryptoString::from(&newspair.get_public_str())
                .expect("Error getting inserting verification key in user_revoke()"),
        );
        map.insert(
            "signing.private",
            CryptoString::from(&newspair.get_private_str())
                .expect("Error getting inserting signing key in user_revoke()"),
        );
        match entry.set_field("Verification-Key", &newspair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting verification key in user_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        // TODO: add preferred algorithm support once supported in eznacl
        let newepair = EncryptionPair::generate("CURVE25519")?;
        map.insert(
            "encryption.public",
            CryptoString::from(&newepair.get_public_str())
                .expect("Error getting inserting encryption key in user_revoke()"),
        );
        map.insert(
            "encryption.private",
            CryptoString::from(&newepair.get_private_str())
                .expect("Error getting inserting decryption key in user_revoke()"),
        );
        match entry.set_field("Encryption-Key", &newepair.get_public_str()) {
            Ok(_) => (),
            Err(e) => {
                return Err(LKCError::ErrProgramException(format!(
                    "Error setting encryption key in user_revoke(): {}",
                    e.to_string()
                )))
            }
        }

        entry.set_field("Revoke", self.get_authstr("Hash")?.as_str())?;
        entry.set_expiration(expires)?;

        Ok((entry, map))
    }
}

// Takes a string containing an index and increments the value inside it, e.g. "21" -> "22"
fn increment_index_string(s: &str) -> Result<String, LKCError> {
    let mut val: u32 = match s.parse::<u32>() {
        Ok(v) => v,
        Err(_) => return Err(LKCError::ErrBadValue),
    };

    val += 1;
    Ok(val.to_string())
}

fn get_offset_date(d: Duration) -> Option<String> {
    let offset_date = Utc::now()
        .date_naive()
        .checked_add_signed(d)
        .expect("Unable to create date 365 days from now");

    Some(offset_date.format("%Y-%m-%d").to_string())
}
