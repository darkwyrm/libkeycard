use crate::base::LKCError;
use chrono::{prelude::*, Duration, NaiveDateTime};
use hex;
use lazy_static::lazy_static;
use rand::prelude::*;
use regex::Regex;
use std::fmt;
use std::str::FromStr;

#[cfg(feature = "use_serde")]
use serde::de::{self, Deserialize, Deserializer, Visitor};
#[cfg(feature = "use_serde")]
use serde::ser::{Serialize, Serializer};

lazy_static! {
    #[doc(hidden)]
    pub static ref RANDOMID_PATTERN: regex::Regex =
        Regex::new(r"^[\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}$")
        .unwrap();

    #[doc(hidden)]
    pub static ref USERID_PATTERN: regex::Regex =
        Regex::new(r"^([\p{L}\p{M}\p{N}\-_]|\.[^.])+$")
        .unwrap();

    #[doc(hidden)]
    pub static ref CONTROL_CHARS_PATTERN: regex::Regex =
        Regex::new(r#"\p{C}"#)
        .unwrap();

    #[doc(hidden)]
    pub static ref DOMAIN_PATTERN: regex::Regex =
        Regex::new(r"^([a-zA-Z0-9\-]+)(\.[a-zA-Z0-9\-]+)*$")
        .unwrap();

}

/// IDType identifies the type of RandomID used
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub enum IDType {
    WorkspaceID,
    UserID,
}

/// The RandomID class is similar to v4 UUIDs. To obtain the maximum amount of entropy, all bits
/// are random and no version information is stored in them. The only null value for the RandomID
/// is all zeroes. Lastly, the only permissible format for the string version of the RandomID
/// has all letters in lowercase and dashes are placed in the same places as for UUIDs.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct RandomID {
    data: String,
}

impl RandomID {
    /// Creates a new populated RandomID
    pub fn generate() -> RandomID {
        let mut rdata: [u8; 16] = [0; 16];
        rand::thread_rng().fill_bytes(&mut rdata[..]);
        let out = RandomID {
            data: format!(
                "{}-{}-{}-{}-{}",
                hex::encode(&rdata[0..4]),
                hex::encode(&rdata[4..6]),
                hex::encode(&rdata[6..8]),
                hex::encode(&rdata[8..10]),
                hex::encode(&rdata[10..])
            ),
        };

        out
    }

    /// Creates a RandomID from an existing string and ensures that formatting is correct.
    pub fn from(data: &str) -> Option<RandomID> {
        if !RANDOMID_PATTERN.is_match(data) {
            return None;
        }

        let mut out = RandomID {
            data: String::from("00000000-0000-0000-0000-000000000000"),
        };
        out.data = data.to_lowercase();

        Some(out)
    }

    /// Creates a RandomID from a Mensago UserID instance if compatible. All RandomIDs are valid
    /// UserIDs, but not the other way around.
    pub fn from_userid(uid: &UserID) -> Option<RandomID> {
        match uid.get_type() {
            IDType::UserID => None,
            IDType::WorkspaceID => Some(RandomID {
                data: uid.to_string(),
            }),
        }
    }

    /// Returns the RandomID as a string
    pub fn as_string(&self) -> &str {
        &self.data
    }
}

impl fmt::Display for RandomID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

impl FromStr for RandomID {
    type Err = ();

    fn from_str(input: &str) -> Result<RandomID, Self::Err> {
        match RandomID::from(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for RandomID {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match RandomID::from(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}

/// A basic data type for housing Mensago user IDs. User IDs on the Mensago platform must be no
/// more than 64 ASCII characters. These characters may be from the following: lowercase a-z,
/// numbers, a dash, or an underscore. Periods may also be used so long as they are not consecutive.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct UserID {
    data: String,
    idtype: IDType,
}

impl UserID {
    /// Creates a UserID from an existing string. If it contains illegal characters, it will
    /// return None. All capital letters will have their case squashed for compliance.
    pub fn from(data: &str) -> Option<UserID> {
        if data.len() > 64 || data.len() == 0 {
            return None;
        }

        if !USERID_PATTERN.is_match(data) {
            return None;
        }

        let mut out = UserID {
            data: String::from(data),
            idtype: IDType::UserID,
        };
        out.data = data.to_lowercase();

        out.idtype = if RANDOMID_PATTERN.is_match(&out.data) {
            IDType::WorkspaceID
        } else {
            IDType::UserID
        };

        Some(out)
    }

    /// Creates a UserID from a workspace ID
    pub fn from_wid(wid: &RandomID) -> UserID {
        UserID {
            data: String::from(wid.as_string()),
            idtype: IDType::WorkspaceID,
        }
    }

    /// Returns the UserID as a string
    pub fn as_string(&self) -> &str {
        &self.data
    }

    /// Returns the type of ID (Workspace ID vs Mensago ID)
    pub fn get_type(&self) -> IDType {
        self.idtype
    }
}

impl fmt::Display for UserID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

impl FromStr for UserID {
    type Err = ();

    fn from_str(input: &str) -> Result<UserID, Self::Err> {
        match UserID::from(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for UserID {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match UserID::from(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}

/// A basic data type for housing Internet domains.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Domain {
    data: String,
}

impl Domain {
    /// Creates a Domain from an existing string. If it contains illegal characters, it will
    /// return None. All capital letters will have their case squashed. This type exists to ensure
    /// that valid domains are used across the library
    pub fn from(data: &str) -> Option<Domain> {
        if !DOMAIN_PATTERN.is_match(data) {
            return None;
        }

        let mut out = Domain {
            data: String::from(data),
        };
        out.data = data.to_lowercase();

        Some(out)
    }

    /// Returns the Domain as a string
    pub fn as_string(&self) -> &str {
        &self.data
    }

    /// Returns the parent domain unless doing so would return a top-level domain, i.e. '.com', in
    /// which case it returns None.
    pub fn parent(&self) -> Option<Domain> {
        let domparts: Vec<&str> = self.data.split(".").collect();
        if domparts.len() < 3 {
            return None;
        }

        return Some(Domain {
            data: String::from(&domparts[1..].join(".")),
        });
    }

    /// Removes a level of subdomains from the item, working much like `parent()` except that the
    /// current object is modified. LKCError::ErrOutOfRange is returned if the call would
    /// result in the Domain object housing just a top-level domain, such as `com`.
    pub fn pop(&mut self) -> Result<(), LKCError> {
        let domparts: Vec<&str> = self.data.split(".").collect();
        if domparts.len() < 3 {
            return Err(LKCError::ErrOutOfRange);
        }

        self.data = String::from(&domparts[1..].join("."));
        Ok(())
    }

    /// Adds a subdomain to the object
    pub fn push(&mut self, subdom: &str) -> Result<(), LKCError> {
        let mut domparts: Vec<&str> = self.data.split(".").collect();
        let subdomparts: Vec<&str> = subdom.split(".").collect();

        for part in subdomparts.iter().rev() {
            domparts.insert(0, part);
        }

        self.data = String::from(&domparts.join("."));

        Ok(())
    }
}

impl fmt::Display for Domain {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

impl FromStr for Domain {
    type Err = ();

    fn from_str(input: &str) -> Result<Domain, Self::Err> {
        match Domain::from(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for Domain {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match Domain::from(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}

/// A basic data type representing a full Mensago address. It is used to ensure passing around
/// valid data within the library.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct MAddress {
    pub uid: UserID,
    pub domain: Domain,
    address: String,
}

impl MAddress {
    /// Creates a new MAddress from a string. If the string does not contain a valid Mensago
    /// address, None will be returned.
    pub fn from(data: &str) -> Option<MAddress> {
        let parts = data.split("/").collect::<Vec<&str>>();

        if parts.len() != 2 {
            return None;
        }

        let out = MAddress {
            uid: UserID::from(parts[0])?,
            domain: Domain::from(parts[1])?,
            address: format!("{}/{}", parts[0], parts[1]),
        };

        Some(out)
    }

    /// Creates an MAddress from an WAddress instance
    pub fn from_waddress(waddr: &WAddress) -> MAddress {
        let uid = UserID::from_wid(&waddr.wid);
        MAddress {
            address: format!("{}/{}", uid, waddr.domain),
            uid,
            domain: waddr.domain.clone(),
        }
    }

    /// Creates an MAddress from its components
    pub fn from_parts(uid: &UserID, domain: &Domain) -> MAddress {
        MAddress {
            uid: uid.clone(),
            domain: domain.clone(),
            address: format!("{}/{}", uid, domain),
        }
    }

    /// Returns the MAddress as a string
    pub fn as_string(&self) -> String {
        format!("{}/{}", self.uid, self.domain)
    }

    /// Returns the UserID portion of the address
    pub fn get_uid(&self) -> &UserID {
        &self.uid
    }

    /// Returns the Domain portion of the address
    pub fn get_domain(&self) -> &Domain {
        &self.domain
    }
}

impl fmt::Display for MAddress {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}/{}", self.uid, self.domain)
    }
}

impl FromStr for MAddress {
    type Err = ();

    fn from_str(input: &str) -> Result<MAddress, Self::Err> {
        match MAddress::from(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for MAddress {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match MAddress::from(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}

/// A basic data type representing a full Mensago address. It is used to ensure passing around
/// valid data within the library.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct WAddress {
    wid: RandomID,
    domain: Domain,

    // We keep this extra copy around because converting the item to a full string is a very
    // common operation
    address: String,
}

impl WAddress {
    /// Creates a new WAddress from a string. If the string does not contain a valid workspace
    /// address, None will be returned.
    pub fn from(data: &str) -> Option<WAddress> {
        let parts = data.split("/").collect::<Vec<&str>>();

        if parts.len() != 2 {
            return None;
        }

        let out = WAddress {
            wid: RandomID::from(parts[0])?,
            domain: Domain::from(parts[1])?,
            address: format!("{}/{}", parts[0], parts[1]),
        };

        Some(out)
    }

    /// Creates a WAddress from an MAddress instance if compatible. This is because all workspace
    /// addresses are valid Mensago addresses, but not the other way around.
    pub fn from_maddress(maddr: &MAddress) -> Option<WAddress> {
        match maddr.uid.get_type() {
            IDType::UserID => None,
            IDType::WorkspaceID => Some(WAddress {
                wid: RandomID::from_userid(&maddr.uid).unwrap(),
                domain: maddr.domain.clone(),
                address: format!("{}/{}", maddr.uid, maddr.domain),
            }),
        }
    }

    /// Creates a WAddress from its components
    pub fn from_parts(wid: &RandomID, domain: &Domain) -> WAddress {
        WAddress {
            wid: wid.clone(),
            domain: domain.clone(),
            address: format!("{}/{}", wid, domain),
        }
    }

    /// Returns the WAddress as a string
    pub fn as_string(&self) -> String {
        format!("{}/{}", self.wid, self.domain)
    }

    /// Returns the RandomID portion of the address
    pub fn get_wid(&self) -> &RandomID {
        &self.wid
    }

    /// Returns the Domain portion of the address
    pub fn get_domain(&self) -> &Domain {
        &self.domain
    }
}

impl fmt::Display for WAddress {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}/{}", self.wid, self.domain)
    }
}

impl FromStr for WAddress {
    type Err = ();

    fn from_str(input: &str) -> Result<WAddress, Self::Err> {
        match WAddress::from(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for WAddress {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match WAddress::from(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}

/// A basic data type representing an Argon2id password hash. It is used to ensure passing around
/// valid data within the library. This might someday be genericized, but for now it's fine.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[cfg_attr(feature = "use_serde", derive(serde::Serialize, serde::Deserialize))]
pub struct ArgonHash {
    hash: String,
    hashtype: String,
}

impl ArgonHash {
    /// Creates a new ArgonHash from the provided password
    pub fn from(password: &str) -> ArgonHash {
        ArgonHash {
            hash: eznacl::hash_password(password, &eznacl::HashStrength::Basic),
            hashtype: String::from("argon2id"),
        }
    }

    /// Creates an ArgonHash object from a verified string
    pub fn from_hashstr(passhash: &str) -> ArgonHash {
        ArgonHash {
            hash: String::from(passhash),
            hashtype: String::from("argon2id"),
        }
    }

    /// Returns the object's hash string
    pub fn get_hash(&self) -> &str {
        &self.hash
    }

    /// Returns the object's hash type
    pub fn get_hashtype(&self) -> &str {
        &self.hashtype
    }
}

impl fmt::Display for ArgonHash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.hash)
    }
}

/// A basic data type for timestamps as used on Mensago. The type is timezone-agnostic as the
/// platform operates on UTC time only. It can be used to just store dates or dates with times
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct Timestamp {
    datetime: NaiveDateTime,
    data: String,
}

impl Timestamp {
    /// Creates a new Timestamp object using the current date and time
    pub fn new() -> Timestamp {
        let utc: DateTime<Utc> = Utc::now();
        let formatted = utc.format("%Y-%m-%dT%H:%M:%SZ");

        Timestamp::from_str(formatted.to_string().as_str()).unwrap()
    }

    /// Creates a new Timestamp from a correctly-formatted string, which is YYYY-MM-DDTHH:MM:SSZ.
    pub fn from_str(data: &str) -> Option<Timestamp> {
        let datetime = match chrono::NaiveDateTime::parse_from_str(data, "%Y-%m-%dT%H:%M:%SZ") {
            Ok(v) => v,
            Err(_) => return None,
        };

        Some(Timestamp {
            datetime,
            data: String::from(data.to_uppercase()),
        })
    }

    /// Creates a timestamp object from just a date, which internally is stored as midnight on
    /// that date.
    pub fn from_datestr(date: &str) -> Option<Self> {
        let datetime = format!("{}T00:00:00Z", date);
        Timestamp::from_str(&datetime)
    }

    /// Returns the timestamp offset by the specified number of days
    pub fn with_offset(&self, days: i64) -> Option<Timestamp> {
        let offset_date = self.datetime.checked_add_signed(Duration::days(days))?;
        Some(Timestamp {
            datetime: offset_date,
            data: offset_date.format("%Y-%m-%dT%H:%M:%SZ").to_string(),
        })
    }

    /// Returns the Timestamp as a string with only the date in the format YYYY-MM-DD.
    pub fn as_datestr(&self) -> &str {
        &self.data[0..10]
    }
}

impl fmt::Display for Timestamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

impl FromStr for Timestamp {
    type Err = ();

    fn from_str(input: &str) -> Result<Timestamp, Self::Err> {
        match Timestamp::from_str(input) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }
}

impl std::convert::TryFrom<&str> for Timestamp {
    type Error = LKCError;
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        match Timestamp::from_str(input) {
            Some(v) => Ok(v),
            None => Err(LKCError::ErrBadValue),
        }
    }
}
#[cfg(feature = "use_serde")]
impl Serialize for Timestamp {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.data.as_str())
    }
}

#[cfg(feature = "use_serde")]
struct TimestampVisitor;

#[cfg(feature = "use_serde")]
impl<'de> Visitor<'de> for TimestampVisitor {
    type Value = Timestamp;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a timestamp in the format YYYYMMDDTHHMMSSZ")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match Timestamp::from_str(value) {
            Some(v) => Ok(v),
            None => Err(E::custom(format!(
                "string is not the expected timestamp format: {}",
                value
            ))),
        }
    }
}

#[cfg(feature = "use_serde")]
impl<'de> Deserialize<'de> for Timestamp {
    fn deserialize<D>(deserializer: D) -> Result<Timestamp, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(TimestampVisitor)
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_randomid() {
        let testid = RandomID::generate();

        let strid = RandomID::from(testid.as_string());
        assert_ne!(strid, None);
    }

    #[test]
    fn test_userid() {
        assert_ne!(UserID::from("valid_e-mail.123"), None);

        match UserID::from("11111111-1111-1111-1111-111111111111") {
            Some(v) => {
                assert!(v.get_type() == IDType::WorkspaceID)
            }
            None => {
                panic!("test_userid failed workspace ID assignment")
            }
        }

        match UserID::from("Valid.but.needs_case-squashed") {
            Some(v) => {
                assert_eq!(v.as_string(), "valid.but.needs_case-squashed")
            }
            None => {
                panic!("test_userid failed case-squashing check")
            }
        }

        assert_eq!(UserID::from("invalid..number1"), None);
        assert_eq!(UserID::from("invalid#2"), None);
    }

    #[test]
    fn test_domain() {
        // Test the basics

        assert!(Domain::from("foo-bar").is_some());
        assert!(Domain::from("foo-bar.baz.com").is_some());

        match Domain::from("FOO.bar.com") {
            Some(v) => {
                assert_eq!(v.as_string(), "foo.bar.com")
            }
            None => {
                panic!("test_domain failed case-squashing check")
            }
        }

        assert!(Domain::from("a bad-id.com").is_none());
        assert!(Domain::from("also_bad.org").is_none());

        // parent() testing

        assert!(Domain::from("foo-bar.baz.com").unwrap().parent().is_some());
        assert!(Domain::from("baz.com").unwrap().parent().is_none());
        assert_eq!(
            Domain::from("foo-bar.baz.com")
                .unwrap()
                .parent()
                .unwrap()
                .to_string(),
            "baz.com"
        );

        // push() and pop()

        let mut d = Domain::from("baz.com").unwrap();
        assert!(d.push("foo.bar").is_ok());
        assert_eq!(d.to_string(), "foo.bar.baz.com");
        assert!(d.pop().is_ok());
        assert_eq!(d.to_string(), "bar.baz.com");
        assert!(d.pop().is_ok());
        assert_eq!(d.to_string(), "baz.com");
        assert!(d.pop().is_err());
    }

    #[test]
    fn test_maddress() {
        assert_ne!(MAddress::from("cats4life/example.com"), None);
        assert_ne!(
            MAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3/example.com"),
            None
        );

        let waddr = WAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3/example.com").unwrap();
        assert_eq!(
            MAddress::from_waddress(&waddr).to_string(),
            "5a56260b-aa5c-4013-9217-a78f094432c3/example.com"
        );

        assert_eq!(MAddress::from("has spaces/example.com"), None);
        assert_eq!(MAddress::from(r#"has_a_"/example.com"#), None);
        assert_eq!(MAddress::from("\\not_allowed/example.com"), None);
        assert_eq!(MAddress::from("/example.com"), None);
        assert_eq!(
            MAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3/example.com/example.com"),
            None
        );
        assert_eq!(MAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3"), None);
    }

    #[test]
    fn test_waddress() {
        assert_ne!(
            WAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3/example.com"),
            None
        );
        assert_eq!(WAddress::from("cats4life/example.com"), None);
        assert_eq!(
            WAddress::from_maddress(&MAddress::from("cats4life/example.com").unwrap()),
            None
        );
        assert!(WAddress::from_maddress(
            &MAddress::from("5a56260b-aa5c-4013-9217-a78f094432c3/example.com").unwrap()
        )
        .is_some());
    }

    #[test]
    fn test_timestamp() {
        assert_eq!(Timestamp::from_str("foobar"), None);

        let ts = Timestamp::from_str("2022-05-01T13:10:11Z").unwrap();
        assert_eq!(&ts.to_string(), "2022-05-01T13:10:11Z");
        assert_eq!(ts.as_datestr(), "2022-05-01");
        assert_eq!(
            &ts.with_offset(1).unwrap().to_string(),
            "2022-05-02T13:10:11Z"
        );

        let ds = Timestamp::from_datestr("2022-05-02").unwrap();
        assert_eq!(ds.as_datestr(), "2022-05-02");
        assert_eq!(&ds.to_string(), "2022-05-02T00:00:00Z");
    }
}
